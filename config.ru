require 'json'
require 'sinatra'
require 'sidekiq'
require 'sidekiq/web'
require './app'
require './workers'

FILENAME = "/Users/adit/Sites/tests/worker_test/tmp_tester.txt"

Sidekiq.configure_client do |config|
  config.redis = { :size => 1 }
end

run Rack::URLMap.new('/' => Sinatra::Application, '/sidekiq' => Sidekiq::Web)



