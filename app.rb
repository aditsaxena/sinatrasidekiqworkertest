get '/workers/clear' do
  Sidekiq.redis { |conn| conn.flushdb }
end

get '/workers/statuses' do
  workers = Sidekiq::Workers.new
  # puts workers.size #  => 2
  
  res = workers.map do |name, work, started_at|
    {
      name: name,
      work: work,
      started_at: started_at,
    }
  end
  
  content_type :json
  res.to_json
end

get '/workers/stats' do
  stats = Sidekiq::Stats.new

  content_type :json  
  {
    processed: stats.processed, # => 100 # number of processed jobs
    failed: stats.failed, # => 3
    queues: stats.queues, # => { "default" => 1001, "email" => 50 }
    enqueued: stats.enqueued, # => 5 # Gets the number of jobs enqueued in all queues (does NOT include retries and scheduled jobs).
  }.to_json
end

get '/' do
  `echo "-----" >> /Users/adit/Sites/tests/worker_test/tmp_tester.txt`
  
  500.times do |i|
    # WorkerTest.new.perform i
    # Sidekiq::Client.enqueue(WorkerTest, i)
    WorkerTest.perform_async(i)
  end
  
  "Workers are loaded #{ rand }<br>"
end